/******************************************************************************
*                                                                             *
* Copyright 2016 myStorm Copyright and related                                *
* rights are licensed under the Solderpad Hardware License, Version 0.51      *
* (the “License”); you may not use this file except in compliance with        *
* the License. You may obtain a copy of the License at                        *
* http://solderpad.org/licenses/SHL-0.51. Unless required by applicable       *
* law or agreed to in writing, software, hardware and materials               *
* distributed under this License is distributed on an “AS IS” BASIS,          *
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or             *
* implied. See the License for the specific language governing                *
* permissions and limitations under the License.                              *
*                                                                             *
******************************************************************************/

module LED_top(
    // 50MHz clock input
    input clk,
    // Input from buttons (active low)
//    input [3:0]sw_n,
    // Outputs to the 8 onboard LEDs
    output[55:0]PMOD,
    );

wire rst = 1'b0;//~sw_n[3]; // make reset active high

// these signals should be high-z when not used
//assign sw1 = 1'bz;
//assign sw2 = 1'bz;
//assign sw3 = 1'bz;

assign PMOD[50:48] = 3'b111;



  blinker awesome_blinker (
    .clk(clk),
    .rst(rst),
    .blink(PMOD[51])
  );

endmodule
