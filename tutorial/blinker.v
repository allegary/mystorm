/******************************************************************************
*                                                                             *
* Copyright 2016 myStorm Copyright and related                                *
* rights are licensed under the Solderpad Hardware License, Version 0.51      *
* (the “License”); you may not use this file except in compliance with        *
* the License. You may obtain a copy of the License at                        *
* http://solderpad.org/licenses/SHL-0.51. Unless required by applicable       *
* law or agreed to in writing, software, hardware and materials               *
* distributed under this License is distributed on an “AS IS” BASIS,          *
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or             *
* implied. See the License for the specific language governing                *
* permissions and limitations under the License.                              *
*                                                                             *
******************************************************************************/

module blinker(input clk, input rst, output blink);
   
	reg [24:0] counter_d, counter_q;
   
	assign blink = counter_q[24];
   
  always @(counter_q) begin
		counter_d = counter_q + 1'b1;
	end
   
	always @(posedge clk) begin
		if (rst) begin
			counter_q <= 25'b0;
		end else begin
			counter_q <= counter_d;
		end
	end
   
endmodule
